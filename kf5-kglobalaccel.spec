%global framework kglobalaccel

Name:           kf5-%{framework}
Version:        5.116.0
Release:        1
Summary:        KDE Frameworks 5 Tier 3 integration module for global shortcuts

License:        CC0-1.0 AND LGPL-2.0-or-later AND LGPL-2.1-only AND LGPL-3.0-only AND (LGPL-2.1-only OR LGPL-3.0-only)
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5

Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-rpm-macros
BuildRequires:  kf5-kconfig-devel >= %{majmin}
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-kcrash-devel >= %{majmin}
BuildRequires:  kf5-kdbusaddons-devel >= %{majmin}
BuildRequires:  kf5-kwindowsystem-devel >= %{majmin}
BuildRequires:  systemd
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  qt5-qtx11extras-devel
BuildRequires:  xcb-util-keysyms-devel
BuildRequires:  libX11-devel
BuildRequires:  libxcb-devel

Requires:       %{name}-libs%{?_isa} = %{version}-%{release}

%description
%{summary}.

%package        libs
Summary:        Runtime libraries for %{name}
Requires:       %{name} = %{version}-%{release}
%description    libs
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
Requires:       qt5-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -p1 -n %{framework}-%{version}


%build
%{cmake_kf5}
%cmake_build


%install
%cmake_install

# unpackaged files
%if 0%{?flatpak:1}
rm -fv %{buildroot}%{_prefix}/lib/systemd/user/plasma-kglobalaccel.service
%endif


%find_lang_kf5 kglobalaccel5_qt

%files -f kglobalaccel5_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}*
%{_kf5_bindir}/kglobalaccel5
%{_kf5_datadir}/kservices5/kglobalaccel5.desktop
%{_datadir}/dbus-1/services/org.kde.kglobalaccel.service
%if ! 0%{?flatpak:1}
%{_userunitdir}/plasma-kglobalaccel.service
%endif

%ldconfig_scriptlets libs

%files libs
%{_kf5_libdir}/libKF5GlobalAccel.so.*
%{_kf5_libdir}/libKF5GlobalAccelPrivate.so.*
%{_kf5_qtplugindir}/org.kde.kglobalaccel5.platforms/

%files devel
%{_kf5_includedir}/KGlobalAccel/
%{_kf5_libdir}/libKF5GlobalAccel.so
%{_kf5_libdir}/cmake/KF5GlobalAccel/
%{_kf5_archdatadir}/mkspecs/modules/qt_KGlobalAccel.pri
%{_kf5_datadir}/dbus-1/interfaces/*


%changelog
* Wed Aug 28 2024 peijiankang<peijiankang@kylinos.cn> - 5.116.0-1
- update to upstream version 5.116.0

* Mon Mar 04 2024 houhongxun <houhongxun@kylinos.cn> - 5.115.0-1
- update version to 5.115.0

* Mon Jan 08 2024 haomimi <haomimi@uniontech.com> - 5.113.0-1
- Update to upstream version 5.113.0

* Thu Aug 03 2023 wangqia <wangqia@uniontech.com> - 5.108.0-1
- Update to upstream version 5.108.0

* Tue Dec 13 2022 liqiuyu <liqiuyu@kylinos.cn> - 5.100.0-1
- Update package to version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 pei-jiankang<peijiankang@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Sun Jan 16 2022 pei-jiankang<peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler

